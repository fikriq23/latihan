
<!DOCTYPE HTML>  
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">

<style>


input[type=text] {
  background-color: #3CBC8D;
  color: white;
  border-radius: 15px;
}


.header {
  background-color: #d633ff;
  padding: 20px;
  text-align: center;
}

body {
  background-image: url("https://previews.123rf.com/images/halfpoint/halfpoint1501/halfpoint150100335/35800791-mix-of-office-supplies-on-a-wooden-table-background-view-from-above-.jpg");
}

  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 100px;
  text-align: center;
}




.button {
  display: inline-block;
  padding: 15px 25px;
  font-size: 24px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #4CAF50;
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}




.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>


<center><div class="header"><h2>Form Izin Santri Glagah</h2></div>
<p><span class="error">* Wajib Di Isi</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  Jam Berangkat: <input type="text" name="email">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Keperluan: <input type="text" name="website">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Keterangan: <textarea name="comment" rows="5" cols="40"></textarea>
  <br><br>
  Divis:
  <input type="radio" name="gender" value="Programmer">Programmer
  <input type="radio" name="gender" value="Imers">Imers
  <input type="radio" name="gender" value="Mutimedia">Multimedia
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="button" name="button" value="Send">  
</form>
</center>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
?>

</body>
</html>


